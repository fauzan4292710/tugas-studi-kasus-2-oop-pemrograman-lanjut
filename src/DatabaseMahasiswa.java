/*
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
 */

import java.util.ArrayList;

public class DatabaseMahasiswa {
    private ArrayList<Mahasiswa> mahasiswas = new ArrayList<>();

    public void tambahMahasiswa(Mahasiswa mahasiswa) {
        mahasiswas.add(mahasiswa);
    }

    public ArrayList<String> getNamas() {
        ArrayList<String> allNamas = new ArrayList<>();
        for (Mahasiswa mhs : mahasiswas) {
            allNamas.addAll(mhs.getNamas());
        }
        return allNamas;
    }

    public ArrayList<String> getNims() {
        ArrayList<String> allNims = new ArrayList<>();
        for (Mahasiswa mhs : mahasiswas) {
            allNims.addAll(mhs.getNims());
        }
        return allNims;
    }

    public ArrayList<String> getAlamats() {
        ArrayList<String> allAlamats = new ArrayList<>();
        for (Mahasiswa mhs : mahasiswas) {
            allAlamats.addAll(mhs.getAlamats());
        }
        return allAlamats;
    }

    public ArrayList<Mahasiswa> getMahasiswas() {
        return mahasiswas;
    }
}
