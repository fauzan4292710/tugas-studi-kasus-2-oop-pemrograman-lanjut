/*
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
 */

import java.util.ArrayList;

public class Mahasiswa {
    private ArrayList<String> namas = new ArrayList<>();
    private ArrayList<String> nims = new ArrayList<>();
    private ArrayList<String> alamats = new ArrayList<>();

    public void tambahData(String nama, String nim, String alamat) {
        namas.add(nama);
        nims.add(nim);
        alamats.add(alamat);
    }

    public void setNama(int index, String nama) {
        if (index >= 0 && index < namas.size()) {
            namas.set(index, nama);
        } else {
            System.out.println("Indeks tidak valid.");
        }
    }

    public void setNim(int index, String nim) {
        if (index >= 0 && index < nims.size()) {
            nims.set(index, nim);
        } else {
            System.out.println("Indeks tidak valid.");
        }
    }

    public void setAlamat(int index, String alamat) {
        if (index >= 0 && index < alamats.size()) {
            alamats.set(index, alamat);
        } else {
            System.out.println("Indeks tidak valid.");
        }
    }

    public ArrayList<String> getNamas() {
        return namas;
    }

    public ArrayList<String> getNims() {
        return nims;
    }

    public ArrayList<String> getAlamats() {
        return alamats;
    }
}
