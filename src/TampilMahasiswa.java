/*
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
 */

import java.util.ArrayList;

public class TampilMahasiswa {
    public static void tampilkanMahasiswa(DatabaseMahasiswa dbMahasiswa) {
        ArrayList<Mahasiswa> mahasiswas = dbMahasiswa.getMahasiswas();

        System.out.println("Daftar Mahasiswa:");
        System.out.println("==================================");
        for (Mahasiswa mhs : mahasiswas) {
            ArrayList<String> namas = mhs.getNamas();
            ArrayList<String> nims = mhs.getNims();
            ArrayList<String> alamats = mhs.getAlamats();

            for (int i = 0; i < namas.size(); i++) {
                System.out.println("Nama: " + namas.get(i));
                System.out.println("NIM: " + nims.get(i));
                System.out.println("Alamat: " + alamats.get(i));
                System.out.println("----------------------------------");
            }
        }
    }
}
