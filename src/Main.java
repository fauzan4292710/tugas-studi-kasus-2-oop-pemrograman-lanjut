/*
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
 */

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        boolean next = true;

        DatabaseMahasiswa dbMahasiswa = new DatabaseMahasiswa();

        while (next) {
            System.out.print("Masukkan nama : ");
            String nama = input.nextLine();

            System.out.print("Masukkan nim : ");
            String nim = input.nextLine();

            System.out.print("Masukkan alamat: ");
            String alamat = input.nextLine();

            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.tambahData(nama, nim, alamat);
            dbMahasiswa.tambahMahasiswa(mahasiswa);

            System.out.print("Tambah lagi? (y/n): ");
            String tambah = input.nextLine();

            if (tambah.equalsIgnoreCase("n")) {
                next = false;
            }
        }

        input.close();
        TampilMahasiswa.tampilkanMahasiswa(dbMahasiswa);
    }
}
